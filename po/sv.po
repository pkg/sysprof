# Swedish translation for sysprof.
# Copyright © 2016-2022 sysprof's COPYRIGHT HOLDER
# This file is distributed under the same license as the sysprof package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2016, 2018, 2019, 2020, 2021, 2022.
# Josef Andersson <l10nl18nsweja@gmail.com>, 2017.
# Luna Jernberg <droidbittin@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: sysprof master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/sysprof/issues\n"
"POT-Creation-Date: 2022-07-23 11:09+0000\n"
"PO-Revision-Date: 2022-09-11 21:38+0200\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: data/org.gnome.Sysprof.appdata.xml.in.in:5
#: data/org.gnome.Sysprof.desktop.in.in:4 src/sysprof/sysprof-application.c:196
#: src/sysprof/sysprof-application.c:321 src/sysprof/sysprof-window.ui:10
msgid "Sysprof"
msgstr "Sysprof"

#: data/org.gnome.Sysprof.appdata.xml.in.in:6
msgid "Profile an application or entire system"
msgstr "Profilera ett program eller hela systemet"

#: data/org.gnome.Sysprof.appdata.xml.in.in:9
msgid "The GNOME Foundation"
msgstr "GNOME Foundation"

#: data/org.gnome.Sysprof.appdata.xml.in.in:12
msgid ""
"Sysprof allows you to profile applications to aid in debugging and "
"optimization."
msgstr ""
"Sysprof låter dig profilera program för att hjälpa till med felsökning och "
"optimering."

#: data/org.gnome.Sysprof.desktop.in.in:5
msgid "Profiler"
msgstr "Profilerare"

#: data/org.gnome.Sysprof.desktop.in.in:6
msgid "Profile an application or entire system."
msgstr "Profilera ett program eller hela systemet."

#: src/libsysprof/sysprof-callgraph-profile.c:447
msgid "Sysprof was unable to generate a callgraph from the system capture."
msgstr "Sysprof kunde inte skapa en anropsgraf från systemfångsten."

#: src/libsysprof/sysprof-perf-source.c:472
#, c-format
msgid "Sysprof failed to find field “%s”."
msgstr "Sysprof misslyckades med att hitta fältet ”%s”."

#: src/libsysprof/sysprof-perf-source.c:485
#, c-format
msgid "Sysprof failed to parse offset for “%s”."
msgstr "Sysprof misslyckades med att tolka position för ”%s”."

#: src/libsysprof/sysprof-perf-source.c:549
#, c-format
msgid "Sysprof failed to get perf_event ID."
msgstr "Sysprof misslyckades med att erhålla perf_event-ID."

#: src/libsysprof/sysprof-perf-source.c:655
#, c-format
msgid "An error occurred while attempting to access performance counters"
msgstr "Ett fel uppstod vid försök att komma åt prestandaräknare"

#: src/libsysprof-ui/sysprof-battery-aid.c:182
msgid "Battery Charge"
msgstr "Batteriladdning"

#: src/libsysprof-ui/sysprof-battery-aid.c:198
msgid "Battery Charge (All)"
msgstr "Batteriladdning (Alla)"

#: src/libsysprof-ui/sysprof-battery-aid.c:240
msgid "Battery"
msgstr "Batteri"

#: src/libsysprof-ui/sysprof-callgraph-aid.c:212
#: src/libsysprof-ui/sysprof-callgraph-aid.c:218
#: src/libsysprof-ui/sysprof-details-page.ui:73
msgid "Stack Traces"
msgstr "Stackspår"

#: src/libsysprof-ui/sysprof-callgraph-aid.c:226
msgid "Stack Traces (In Kernel)"
msgstr "Stackspår (i kärna)"

#: src/libsysprof-ui/sysprof-callgraph-aid.c:234
msgid "Stack Traces (In User)"
msgstr "Stackspår (i användare)"

#: src/libsysprof-ui/sysprof-callgraph-aid.c:243
#: src/libsysprof-ui/sysprof-callgraph-aid.c:273
msgid "Callgraph"
msgstr "Anropsgraf"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:23
#: src/libsysprof-ui/sysprof-memprof-page.ui:90
msgid "Functions"
msgstr "Funktioner"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:39
#: src/libsysprof-ui/sysprof-callgraph-page.ui:96
#: src/libsysprof-ui/sysprof-callgraph-page.ui:147
#: src/libsysprof-ui/sysprof-memprof-page.ui:107
#: src/libsysprof-ui/sysprof-memprof-page.ui:165
#: src/libsysprof-ui/sysprof-memprof-page.ui:216
msgid "Self"
msgstr "Själv"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:55
#: src/libsysprof-ui/sysprof-callgraph-page.ui:112
#: src/libsysprof-ui/sysprof-callgraph-page.ui:163
#: src/libsysprof-ui/sysprof-memprof-page.ui:123
#: src/libsysprof-ui/sysprof-memprof-page.ui:181
#: src/libsysprof-ui/sysprof-memprof-page.ui:232
msgid "Total"
msgstr "Totalt"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:80
#: src/libsysprof-ui/sysprof-memprof-page.ui:148
msgid "Callers"
msgstr "Anropare"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:139
#: src/libsysprof-ui/sysprof-memprof-page.ui:207
msgid "Descendants"
msgstr "Ättlingar"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:178
#: src/libsysprof-ui/sysprof-details-page.ui:189
msgid "Hits"
msgstr "Träffar"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:198
msgid "Generating Callgraph"
msgstr "Genererar anropsgraf"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:199
msgid "Sysprof is busy creating the selected callgraph."
msgstr "Sysprof är upptaget med att skapa vald anropsgraf."

#: src/libsysprof-ui/sysprof-callgraph-page.ui:205
#: src/libsysprof-ui/sysprof-memprof-page.ui:271
msgid "Not Enough Samples"
msgstr "För få samplingar"

#: src/libsysprof-ui/sysprof-callgraph-page.ui:206
#: src/libsysprof-ui/sysprof-memprof-page.ui:272
msgid "More samples are necessary to display a callgraph."
msgstr "Fler samplingar är nödvändiga för att skapa en anropsgraf."

#: src/libsysprof-ui/sysprof-cell-renderer-progress.c:102
#: src/libsysprof-ui/sysprof-cell-renderer-progress.c:292
#: src/libsysprof-ui/sysprof-cell-renderer-progress.c:322
#, c-format
msgctxt "progress bar label"
msgid "%d %%"
msgstr "%d %%"

#: src/libsysprof-ui/sysprof-counters-aid.c:221
#: src/libsysprof-ui/sysprof-counters-aid.c:226
#: src/libsysprof-ui/sysprof-counters-aid.c:282
#: src/libsysprof-ui/sysprof-details-page.ui:148
msgid "Counters"
msgstr "Räknare"

#: src/libsysprof-ui/sysprof-cpu-aid.c:207
#: src/libsysprof-ui/sysprof-cpu-aid.c:355
msgid "CPU Usage"
msgstr "Processoranvändning"

#: src/libsysprof-ui/sysprof-cpu-aid.c:214
msgid "CPU Frequency"
msgstr "Processorfrekvens"

#: src/libsysprof-ui/sysprof-cpu-aid.c:218
msgid "CPU Frequency (All)"
msgstr "Processorfrekvens (alla)"

#. Translators: CPU is the processor.
#: src/libsysprof-ui/sysprof-cpu-aid.c:227
#: src/libsysprof-ui/sysprof-cpu-aid.c:250
msgid "CPU Usage (All)"
msgstr "Processoranvändning (alla)"

#: src/libsysprof-ui/sysprof-cpu-aid.c:316
#: src/libsysprof-ui/sysprof-details-page.ui:103
msgid "Processes"
msgstr "Processer"

#: src/libsysprof-ui/sysprof-details-page.c:231
msgid "Memory Capture"
msgstr "Minnesfångst"

#: src/libsysprof-ui/sysprof-details-page.c:245
#, c-format
msgid "%0.4lf seconds"
msgstr "%0.4lf sekunder"

#: src/libsysprof-ui/sysprof-details-page.ui:8
msgid "Capture"
msgstr "Fångst"

#: src/libsysprof-ui/sysprof-details-page.ui:12
msgid "Location"
msgstr "Plats"

#: src/libsysprof-ui/sysprof-details-page.ui:26
msgid "Recorded At"
msgstr "Inspelad"

#: src/libsysprof-ui/sysprof-details-page.ui:40
#: src/libsysprof-ui/sysprof-marks-page.ui:90
#: src/libsysprof-ui/sysprof-marks-page.ui:196
msgid "Duration"
msgstr "Längd"

#: src/libsysprof-ui/sysprof-details-page.ui:54
msgid "CPU Model"
msgstr "Processormodell"

#: src/libsysprof-ui/sysprof-details-page.ui:69
#: src/libsysprof-ui/sysprof-details-page.ui:164
msgid "Statistics"
msgstr "Statistik"

#: src/libsysprof-ui/sysprof-details-page.ui:74
msgid "Number of stack traces sampled for performance callgraphs"
msgstr "Antalet stackspår som provtagits för prestandaanropsgrafer"

#: src/libsysprof-ui/sysprof-details-page.ui:88
msgid "Marks"
msgstr "Märken"

#: src/libsysprof-ui/sysprof-details-page.ui:89
msgid "Number of marks seen"
msgstr "Antalet märken som setts"

#: src/libsysprof-ui/sysprof-details-page.ui:104
msgid "Number of processes seen"
msgstr "Antalet processer som setts"

#: src/libsysprof-ui/sysprof-details-page.ui:118
msgid "Forks"
msgstr "Fork-anrop"

#: src/libsysprof-ui/sysprof-details-page.ui:119
msgid "Number of times a process forked"
msgstr "Antalet gånger en process har förgrenats med fork"

#: src/libsysprof-ui/sysprof-details-page.ui:133
#: src/libsysprof-ui/sysprof-memprof-aid.c:193
#: src/libsysprof-ui/sysprof-memprof-visualizer.c:113
msgid "Memory Allocations"
msgstr "Minnesallokeringar"

#: src/libsysprof-ui/sysprof-details-page.ui:134
msgid "Number of stack traces recorded for tracing memory allocations"
msgstr "Antalet stackspår som inspelats för spårning av minnesallokeringar"

#: src/libsysprof-ui/sysprof-details-page.ui:149
msgid "Number of recorded counter values"
msgstr "Antalet inspelade räknarvärden"

#: src/libsysprof-ui/sysprof-details-page.ui:176
#: src/libsysprof-ui/sysprof-marks-page.ui:50
msgid "Mark"
msgstr "Märke"

#: src/libsysprof-ui/sysprof-details-page.ui:202
msgid "Min"
msgstr "Min"

#: src/libsysprof-ui/sysprof-details-page.ui:215
msgid "Max"
msgstr "Max"

#: src/libsysprof-ui/sysprof-details-page.ui:228
msgid "Avg"
msgstr "Mdl"

#: src/libsysprof-ui/sysprof-diskstat-aid.c:205
#: src/libsysprof-ui/sysprof-diskstat-aid.c:267
msgid "Disk"
msgstr "Disk"

#: src/libsysprof-ui/sysprof-diskstat-aid.c:239
msgid "Reads"
msgstr "Läsningar"

#: src/libsysprof-ui/sysprof-diskstat-aid.c:239
msgid "Writes"
msgstr "Skrivningar"

#: src/libsysprof-ui/sysprof-display.c:228
msgid "Recording Failed"
msgstr "Inspelning misslyckades"

#: src/libsysprof-ui/sysprof-display.c:233
msgid "Recording…"
msgstr "Spelar in…"

#. translators: %s is replaced with locale specific time of recording
#: src/libsysprof-ui/sysprof-display.c:264
#, c-format
msgid "Recording at %s"
msgstr "Inspelning från %s"

#: src/libsysprof-ui/sysprof-display.c:268
msgid "New Recording"
msgstr "Ny inspelning"

#: src/libsysprof-ui/sysprof-display.c:1096
msgid "The recording could not be opened"
msgstr "Inspelningen kunde inte öppnas"

#: src/libsysprof-ui/sysprof-display.c:1243
#, c-format
msgid "Failed to save recording: %s"
msgstr "Misslyckades med att spara inspelning: %s"

#: src/libsysprof-ui/sysprof-display.c:1276
msgid "Save Recording"
msgstr "Spara inspelning"

#: src/libsysprof-ui/sysprof-display.c:1279
msgid "Save"
msgstr "Spara"

#. Translators: This is a button.
#: src/libsysprof-ui/sysprof-display.c:1280 src/sysprof/sysprof-window.c:285
msgid "Cancel"
msgstr "Avbryt"

#: src/libsysprof-ui/sysprof-display.ui:36
#: src/libsysprof-ui/sysprof-marks-page.ui:16
msgid "Details"
msgstr "Detaljer"

#: src/libsysprof-ui/sysprof-environ-editor-row.ui:29
msgid "Remove environment variable"
msgstr "Ta bort miljövariabel"

#: src/libsysprof-ui/sysprof-environ-editor.c:72
msgid "New environment variable…"
msgstr "Ny miljövariabel…"

#: src/libsysprof-ui/sysprof-failed-state-view.ui:27
msgid "Ouch, that hurt!"
msgstr "Aj, det där gjorde ont!"

#: src/libsysprof-ui/sysprof-failed-state-view.ui:40
msgid "Something unexpectedly went wrong while trying to profile your system."
msgstr "Något gick oväntat fel då ditt system skulle profileras."

#: src/libsysprof-ui/sysprof-log-model.c:210
#: src/libsysprof-ui/sysprof-logs-page.ui:58
#: src/libsysprof-ui/sysprof-marks-page.ui:103
msgid "Message"
msgstr "Meddelande"

#: src/libsysprof-ui/sysprof-log-model.c:213
msgid "Info"
msgstr "Info"

#: src/libsysprof-ui/sysprof-log-model.c:216
msgid "Critical"
msgstr "Kritisk"

#: src/libsysprof-ui/sysprof-log-model.c:219
msgid "Error"
msgstr "Fel"

#: src/libsysprof-ui/sysprof-log-model.c:222
msgid "Debug"
msgstr "Felsökning"

#: src/libsysprof-ui/sysprof-log-model.c:225
msgid "Warning"
msgstr "Varning"

#: src/libsysprof-ui/sysprof-logs-aid.c:195
#: src/libsysprof-ui/sysprof-logs-aid.c:199
#: src/libsysprof-ui/sysprof-logs-aid.c:204
#: src/libsysprof-ui/sysprof-logs-aid.c:210
msgid "Logs"
msgstr "Loggar"

#: src/libsysprof-ui/sysprof-logs-page.ui:15
#: src/libsysprof-ui/sysprof-marks-page.ui:64
msgid "Time"
msgstr "Tid"

#: src/libsysprof-ui/sysprof-logs-page.ui:29
msgid "Severity"
msgstr "Allvarsgrad"

#: src/libsysprof-ui/sysprof-logs-page.ui:44
msgid "Domain"
msgstr "Domän"

#: src/libsysprof-ui/sysprof-marks-aid.c:408
#: src/libsysprof-ui/sysprof-marks-aid.c:413
msgid "Timings"
msgstr "Tidsvärden"

#: src/libsysprof-ui/sysprof-marks-page.ui:37
msgid "Group"
msgstr "Grupp"

#: src/libsysprof-ui/sysprof-marks-page.ui:77
msgid "End"
msgstr "Slut"

#: src/libsysprof-ui/sysprof-marks-page.ui:234
msgid "No Timings Available"
msgstr "Inga tidsvärden tillgängliga"

#: src/libsysprof-ui/sysprof-marks-page.ui:246
msgid "No timing data was found for the current selection"
msgstr "Inga tidsdata hittades för den aktuella markeringen"

#: src/libsysprof-ui/sysprof-memory-aid.c:68
msgid "Memory Usage"
msgstr "Minnesanvändning"

#: src/libsysprof-ui/sysprof-memprof-aid.c:182
msgid "Memory"
msgstr "Minne"

#: src/libsysprof-ui/sysprof-memprof-aid.c:224
msgid "Track Allocations"
msgstr "Spåra allokeringar"

#. translators: %s is replaced with the the lower and upper bound memory sizes in bytes
#: src/libsysprof-ui/sysprof-memprof-page.c:246
#, c-format
msgid "> %s to %s"
msgstr "> %s till %s"

#: src/libsysprof-ui/sysprof-memprof-page.ui:16
msgid "Number of Allocations"
msgstr "Antal allokeringar"

#: src/libsysprof-ui/sysprof-memprof-page.ui:17
msgid "Total number of allocation and free records"
msgstr "Totala antalet allokeringar och frigöranden"

#: src/libsysprof-ui/sysprof-memprof-page.ui:30
#: src/libsysprof-ui/sysprof-memprof-page.ui:318
msgid "Leaked Allocations"
msgstr "Läckta allokeringar"

#: src/libsysprof-ui/sysprof-memprof-page.ui:31
msgid "Number of allocations without a free record"
msgstr "Antal allokeringar utan ett frigörande"

#: src/libsysprof-ui/sysprof-memprof-page.ui:44
#: src/libsysprof-ui/sysprof-memprof-page.ui:311
msgid "Temporary Allocations"
msgstr "Tillfälliga allokeringar"

#: src/libsysprof-ui/sysprof-memprof-page.ui:45
msgid "Number of allocations freed from similar stack trace"
msgstr "Antal allokeringar frigjorda från liknande stackspår"

#: src/libsysprof-ui/sysprof-memprof-page.ui:59
msgid "Allocations by Size"
msgstr "Allokeringar efter storlek"

#: src/libsysprof-ui/sysprof-memprof-page.ui:247
msgid "Size"
msgstr "Storlek"

#: src/libsysprof-ui/sysprof-memprof-page.ui:264
msgid "Analyzing Memory Allocations"
msgstr "Analyserar minnesallokeringar"

#: src/libsysprof-ui/sysprof-memprof-page.ui:265
msgid "Sysprof is busy analyzing memory allocations."
msgstr "Sysprof är upptaget med att analysera minnesallokeringar."

#: src/libsysprof-ui/sysprof-memprof-page.ui:298
msgid "Summary"
msgstr "Sammanfattning"

#: src/libsysprof-ui/sysprof-memprof-page.ui:304
msgid "All Allocations"
msgstr "Alla allokeringar"

#: src/libsysprof-ui/sysprof-memprof-visualizer.c:113
msgid "Memory Used"
msgstr "Minne använt"

#: src/libsysprof-ui/sysprof-netdev-aid.c:204
#: src/libsysprof-ui/sysprof-netdev-aid.c:264
msgid "Network"
msgstr "Nätverk"

#. translators: "Compositor" means desktop compositor, gnome-shell/mutter in particular
#: src/libsysprof-ui/sysprof-profiler-assistant.ui:26
msgid "GNOME Shell"
msgstr "GNOME Shell"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:69
msgid "Profiling Target"
msgstr "Profileringsmål"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:72
msgid "Profile Entire System"
msgstr "Profilera hela systemet"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:73
msgid ""
"Sysprof can generate callgraphs for one or more processes on your system."
msgstr ""
"Sysprof kan skapa anropsgrafer för en eller flera processer på ditt system."

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:84
msgid "Search Processes"
msgstr "Sök processer"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:99
msgid "Loading Processes…"
msgstr "Läser in processer…"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:114
msgid "Launch Application"
msgstr "Starta program"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:115
msgid ""
"Sysprof can launch an application to be profiled. The profiler will "
"automatically stop when it exits."
msgstr ""
"Sysprof kan starta ett program som ska profileras. Profileraren kommer "
"automatiskt stoppas när det avslutas."

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:126
msgid "Command Line"
msgstr "Kommandorad"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:135
msgid "Inherit Environment"
msgstr "Ärv miljö"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:136
msgid ""
"Enable to ensure your application shares the display, message-bus, and other "
"desktop environment settings."
msgstr ""
"Aktivera för att säkerställa att ditt program delar display, meddelandebuss "
"och andra skrivbordsmiljöinställningar."

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:153
msgid "Performance"
msgstr "Prestanda"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:156
msgid "Allow CPU Throttling"
msgstr "Tillåt processorstrypning"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:157
msgid ""
"When enabled, your system is allowed to scale CPU frequency as necessary."
msgstr "När aktiverat får ditt system skala processorfrekvens efter behov."

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:172
#: src/libsysprof-ui/sysprof-visualizers-frame.ui:27
msgid "Instruments"
msgstr "Instrument"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:245
msgid ""
"Track application memory allocations (Sysprof must launch target application)"
msgstr ""
"Spåra minnesallokeringar för program (Sysprof måste starta målprogrammet)"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:252
msgid "Track slow operations on your applications main loop"
msgstr "Spåra långsamma operationer på ditt programs huvudslinga"

#: src/libsysprof-ui/sysprof-profiler-assistant.ui:265
msgid "_Record"
msgstr "_Spela in"

#: src/libsysprof-ui/sysprof-rapl-aid.c:175
#: src/libsysprof-ui/sysprof-rapl-aid.c:248
msgid "Energy Usage"
msgstr "Energianvändning"

#: src/libsysprof-ui/sysprof-rapl-aid.c:180
msgid "Energy Usage (All)"
msgstr "Energianvändning (Alla)"

#: src/libsysprof-ui/sysprof-recording-state-view.ui:35
msgid ""
"Did you know you can use <a href=\"help:sysprof\">sysprof-cli</a> to record?"
msgstr ""
"Visste du att du kan använda <a href=\"help:sysprof\">sysprof-cli</a> för "
"att spela in?"

#: src/libsysprof-ui/sysprof-recording-state-view.ui:50
msgid "Events"
msgstr "Händelser"

#: src/libsysprof-ui/sysprof-recording-state-view.ui:76
msgid "_Stop Recording"
msgstr "_Sluta spela in"

#: src/libsysprof-ui/sysprof-visualizer-group-header.c:136
#: src/libsysprof-ui/sysprof-visualizers-frame.ui:41
msgid "Select for more details"
msgstr "Välj för mer detaljer"

#: src/libsysprof-ui/sysprof-visualizer-group-header.c:158
msgid "Display supplemental graphs"
msgstr "Visa stöddiagram"

#: src/sysprofd/org.gnome.sysprof3.policy.in:13
msgid "Profile the system"
msgstr "Profilera systemet"

#: src/sysprofd/org.gnome.sysprof3.policy.in:14
msgid "Authentication is required to profile the system."
msgstr "Autentisering krävs för att profilera systemet."

#: src/sysprofd/org.gnome.sysprof3.policy.in:26
msgid "Open a perf event stream"
msgstr "Öppna en händelseström för perf"

#: src/sysprofd/org.gnome.sysprof3.policy.in:27
msgid "Authentication is required to access system performance counters."
msgstr "Autentisering krävs för att komma åt systemprestandaräknare."

#: src/sysprofd/org.gnome.sysprof3.policy.in:37
msgid "Get a list of kernel symbols and their address"
msgstr "Erhåll en lista över kärnsymboler och deras adresser"

#: src/sysprofd/org.gnome.sysprof3.policy.in:38
msgid "Authentication is required to access Linux kernel information."
msgstr "Autentisering krävs för att komma åt Linux-kärninformation."

#: src/sysprof/gtk/help-overlay.ui:8
msgctxt "shortcut window"
msgid "Sysprof Shortcuts"
msgstr "Sysprof-genvägar"

#: src/sysprof/gtk/help-overlay.ui:12
msgctxt "shortcut window"
msgid "Files"
msgstr "Filer"

#: src/sysprof/gtk/help-overlay.ui:16
msgctxt "shortcut window"
msgid "Save Recording"
msgstr "Spara inspelning"

#: src/sysprof/gtk/help-overlay.ui:17
msgctxt "shortcut window"
msgid "Saves the current recording"
msgstr "Sparar den aktuella inspelningen"

#: src/sysprof/gtk/help-overlay.ui:24
msgctxt "shortcut window"
msgid "Open recording"
msgstr "Öppna inspelning"

#: src/sysprof/gtk/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Opens a previously saved recording"
msgstr "Öppnar en tidigare sparad inspelning"

#: src/sysprof/gtk/help-overlay.ui:34
msgctxt "shortcut window"
msgid "Recording"
msgstr "Spelar in"

#: src/sysprof/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Record again"
msgstr "Spela in igen"

#: src/sysprof/gtk/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Starts a new recording"
msgstr "Startar en ny inspelning"

#: src/sysprof/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Stop recording"
msgstr "Sluta spela in"

#: src/sysprof/gtk/help-overlay.ui:55
msgctxt "shortcut window"
msgid "Callgraph"
msgstr "Anropsgraf"

#: src/sysprof/gtk/help-overlay.ui:59
msgctxt "shortcut window"
msgid "Expand function"
msgstr "Utöka funktionen"

#: src/sysprof/gtk/help-overlay.ui:60
msgctxt "shortcut window"
msgid "Shows the direct descendants of the callgraph function"
msgstr "Visar direkta arvingar för anropsgrafsfunktionen"

#: src/sysprof/gtk/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Collapse function"
msgstr "Fäll ihop funktionen"

#: src/sysprof/gtk/help-overlay.ui:68
msgctxt "shortcut window"
msgid "Hides all callgraph descendants below the selected function"
msgstr "Döljer alla anropsgrafsarvingar under den valda funktionen"

#: src/sysprof/gtk/help-overlay.ui:75
msgctxt "shortcut window"
msgid "Jump into function"
msgstr "Hoppa in i funktion"

#: src/sysprof/gtk/help-overlay.ui:76
msgctxt "shortcut window"
msgid "Selects the function or file as the top of the callgraph"
msgstr "Markerar funktionen eller filen som toppen på dess anropsgraf"

#: src/sysprof/gtk/help-overlay.ui:85
msgctxt "shortcut window"
msgid "Visualizers"
msgstr "Visualiserare"

#: src/sysprof/gtk/help-overlay.ui:89
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Zooma in"

#: src/sysprof/gtk/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Zooma ut"

#: src/sysprof/gtk/help-overlay.ui:103
msgctxt "shortcut window"
msgid "Reset zoom"
msgstr "Återställ zoom"

#: src/sysprof/gtk/menus.ui:6
msgid "New Tab"
msgstr "Ny flik"

#: src/sysprof/gtk/menus.ui:11
msgid "New Window"
msgstr "Nytt fönster"

#: src/sysprof/gtk/menus.ui:18
msgid "Open Recording…"
msgstr "Öppna inspelning…"

#: src/sysprof/gtk/menus.ui:23
msgid "Save Recording…"
msgstr "Spara inspelning…"

#: src/sysprof/gtk/menus.ui:30
msgid "Record Again"
msgstr "Spela in igen"

#: src/sysprof/gtk/menus.ui:37
msgid "Close"
msgstr "Stäng"

#: src/sysprof/gtk/menus.ui:44
msgid "Keyboard Shortcuts"
msgstr "Tangentbordsgenvägar"

#: src/sysprof/gtk/menus.ui:49
msgid "Help"
msgstr "Hjälp"

#: src/sysprof/gtk/menus.ui:54
msgid "About Sysprof"
msgstr "Om Sysprof"

#: src/sysprof/sysprof-application.c:205
msgid "A system profiler"
msgstr "En systemprofilerare"

#: src/sysprof/sysprof-application.c:206
msgid "translator-credits"
msgstr ""
"Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Luna Jernberg <droidbittin@gmail.com>\n"
"Skicka synpunkter på översättningen till\n"
"<tp-sv@listor.tp-sv.se>."

#. Translators: This is a window title.
#: src/sysprof/sysprof-window.c:279
msgid "Open Capture…"
msgstr "Öppna fångst…"

#. Translators: This is a button.
#: src/sysprof/sysprof-window.c:283
msgid "Open"
msgstr "Öppna"

#: src/sysprof/sysprof-window.c:288
msgid "Sysprof Captures"
msgstr "Sysprof-fångster"

#: src/sysprof/sysprof-window.c:293
msgid "All Files"
msgstr "Alla filer"

#: src/sysprof/sysprof-window.ui:34
msgid "_Open"
msgstr "Ö_ppna"

#: src/sysprof/sysprof-window.ui:37
msgid "Open Recording… (Ctrl+O)"
msgstr "Öppna inspelning… (Ctrl+O)"

#: src/tools/sysprof-cli.c:62
msgid "Stopping profiler. Press twice more ^C to force exit."
msgstr "Stoppar profilerare. Tryck ^C två gånger till för att tvinga avslut."

#: src/tools/sysprof-cli.c:75
msgid "Profiler stopped."
msgstr "Profilerare stoppades."

#: src/tools/sysprof-cli.c:108
msgid "--merge requires at least 2 filename arguments"
msgstr "--merge kräver minst 2 filnamnsargument"

#: src/tools/sysprof-cli.c:226
msgid "Disable CPU throttling while profiling"
msgstr "Inaktivera processorstrypning under profilering"

#: src/tools/sysprof-cli.c:227
msgid "Make sysprof specific to a task"
msgstr "Gör sysprof specifik för en uppgift"

#: src/tools/sysprof-cli.c:227
msgid "PID"
msgstr "PID"

#: src/tools/sysprof-cli.c:228
msgid "Run a command and profile the process"
msgstr "Kör ett kommando och profilera processen"

#: src/tools/sysprof-cli.c:228
msgid "COMMAND"
msgstr "KOMMANDO"

#: src/tools/sysprof-cli.c:229
msgid ""
"Set environment variable for spawned process. Can be used multiple times."
msgstr "Ställ in miljövariabel för startad process. Kan användas flera gånger."

#: src/tools/sysprof-cli.c:229
msgid "VAR=VALUE"
msgstr "VAR=VÄRDE"

#: src/tools/sysprof-cli.c:230
msgid "Force overwrite the capture file"
msgstr "Tvinga överskrivning av fångstfilen"

#: src/tools/sysprof-cli.c:231
msgid "Disable recording of battery statistics"
msgstr "Inaktivera inspelning av batteristatistik"

#: src/tools/sysprof-cli.c:232
msgid "Disable recording of CPU statistics"
msgstr "Inaktivera inspelning av processorstatistik"

#: src/tools/sysprof-cli.c:233
msgid "Disable recording of Disk statistics"
msgstr "Inaktivera inspelning av diskstatistik"

#: src/tools/sysprof-cli.c:234
msgid "Do not record stacktraces using Linux perf"
msgstr "Spela inte in stackspår med Linux perf"

#: src/tools/sysprof-cli.c:235
msgid "Do not append symbol name information from local machine"
msgstr "Lägg inte till symbolnamnsinformation från lokal maskin"

#: src/tools/sysprof-cli.c:236
msgid "Disable recording of memory statistics"
msgstr "Inaktivera inspelning av minnesstatistik"

#: src/tools/sysprof-cli.c:237
msgid "Disable recording of network statistics"
msgstr "Inaktivera inspelning av nätverksstatistik"

#: src/tools/sysprof-cli.c:238
msgid "Set SYSPROF_TRACE_FD environment for subprocess"
msgstr "Ställ in SYSPROF_TRACE_FD-miljö för underprocess"

#: src/tools/sysprof-cli.c:239
msgid "Set GJS_TRACE_FD environment to trace GJS processes"
msgstr "Ställ in GJS_TRACE_FD-miljö för att spåra GJS-processer"

#: src/tools/sysprof-cli.c:240
msgid "Set GTK_TRACE_FD environment to trace a GTK application"
msgstr "Ställ in GTK_TRACE_FD-miljö för att spåra ett GTK-program"

#: src/tools/sysprof-cli.c:241
msgid "Include RAPL energy statistics"
msgstr "Inkludera RAPL-energistatistik"

#: src/tools/sysprof-cli.c:242
msgid "Profile memory allocations and frees"
msgstr "Profilera minnesallokeringar och frigöranden"

#: src/tools/sysprof-cli.c:243
msgid "Connect to org.gnome.Shell for profiler statistics"
msgstr "Anslut till org.gnome.Shell för profilerarstatistik"

#: src/tools/sysprof-cli.c:244
msgid "Track performance of the applications main loop"
msgstr "Spåra prestanda på programmets huvudslinga"

#: src/tools/sysprof-cli.c:245
msgid "Merge all provided *.syscap files and write to stdout"
msgstr ""
"Slå samman alla tillhandahållna *.syscap-filer och skriv till standard ut"

#: src/tools/sysprof-cli.c:246
msgid "Print the sysprof-cli version and exit"
msgstr "Skriv ut version för sysprof-cli och avsluta"

#: src/tools/sysprof-cli.c:279
msgid "[CAPTURE_FILE] [-- COMMAND ARGS] — Sysprof"
msgstr "[FÅNGSTFIL] [-- KOMMANDO ARGUMENT] — Sysprof"

#: src/tools/sysprof-cli.c:282
msgid ""
"\n"
"Examples:\n"
"\n"
"  # Record gtk4-widget-factory using trace-fd to get application provided\n"
"  # data as well as GTK and GNOME Shell data providers\n"
"  sysprof-cli --gtk --gnome-shell --use-trace-fd -- gtk4-widget-factory\n"
"\n"
"  # Merge multiple syscap files into one\n"
"  sysprof-cli --merge a.syscap b.syscap > c.syscap\n"
msgstr ""
"\n"
"Exempel:\n"
"\n"
"  # Spela in gtk4-widget-factory med trace-fd för att få data från program\n"
"  # så väl som GTK- och GNOME Shell-dataleverantörer\n"
"  sysprof-cli --gtk --gnome-shell --use-trace-fd -- gtk4-widget-factory\n"
"\n"
"  # Slå samman flera syscap-filer till en\n"
"  sysprof-cli --merge a.syscap b.syscap > c.syscap\n"

#: src/tools/sysprof-cli.c:315
msgid "Too many arguments were passed to sysprof-cli:"
msgstr "För många argument skickades till sysprof-cli:"

#. Translators: %s is a file name.
#: src/tools/sysprof-cli.c:376
#, c-format
msgid "%s exists. Use --force to overwrite\n"
msgstr "%s existerar. Använd --force för att skriva över\n"

#: src/tools/sysprof-profiler-ctl.c:45
msgid "Connect to the system bus"
msgstr "Anslut till systembussen"

#: src/tools/sysprof-profiler-ctl.c:46
msgid "Connect to the session bus"
msgstr "Anslut till sessionsbussen"

#: src/tools/sysprof-profiler-ctl.c:47
msgid "Connect to the given D-Bus address"
msgstr "Anslut till den angivna D-Bus-adressen"

#: src/tools/sysprof-profiler-ctl.c:48
msgid "Destination D-Bus name to invoke method on"
msgstr "Namn på D-Bus-mål att anropa metod på"

#: src/tools/sysprof-profiler-ctl.c:49
msgid "Object path to invoke method on"
msgstr "Objektsökväg att anropa metod på"

#: src/tools/sysprof-profiler-ctl.c:49
msgid "/org/gnome/Sysprof3/Profiler"
msgstr "/org/gnome/Sysprof3/Profiler"

#: src/tools/sysprof-profiler-ctl.c:50
msgid "Timeout in seconds"
msgstr "Tidsgräns i sekunder"

#: src/tools/sysprof-profiler-ctl.c:51
msgid "Overwrite FILENAME if it exists"
msgstr "Skriv över FILNAMN om det existerar"

#: src/tools/sysprof-profiler-ctl.c:79
msgid "--dest=BUS_NAME [FILENAME] - connect to an embedded sysprof profiler"
msgstr ""
"--dest=BUSSNAMN [FILNAMN] - anslut till en inbäddad sysprof-profilerare"

#~ msgid "Last Spawn Program"
#~ msgstr "Senast startade program"

#~ msgid ""
#~ "The last spawned program, which will be set in the UI upon restart of the "
#~ "application."
#~ msgstr ""
#~ "Det senast startade programmet, vilket kommer ställas in i "
#~ "användargränssnittet då programmet startas om."

#~ msgid "Last Spawn Inherit Environment"
#~ msgstr "Senast startad ärver miljö"

#~ msgid "If the last spawned environment inherits the parent environment."
#~ msgstr "Om den senast startade miljön ärver den överordnade miljön."

#~ msgid "Last Spawn Environment"
#~ msgstr "Senast startade miljö"

#~ msgid ""
#~ "The last spawned environment, which will be set in the UI upon restart of "
#~ "the application."
#~ msgstr ""
#~ "Den senast startade miljön, vilken kommer ställas in i "
#~ "användargränssnittet då programmet startas om."

#~ msgid "Filename"
#~ msgstr "Filnamn"

#~ msgid "Samples Captured"
#~ msgstr "Samplingar fångade"

#~ msgid "Marks Captured"
#~ msgstr "Märken fångade"

#~ msgid "Processes Captured"
#~ msgstr "Processer fångade"

#~ msgid "Forks Captured"
#~ msgstr "Fork-anrop fångade"

#~ msgid "Counters Captured"
#~ msgstr "Räknare fångade"

#~ msgid "Learn more about Sysprof"
#~ msgstr "Lär dig mer om Sysprof"

#~ msgid "New variable…"
#~ msgstr "Ny variabel…"

#~ msgid "Profilers"
#~ msgstr "Profilerare"

#~ msgid "All Processes"
#~ msgstr "Alla processer"

#~ msgid ""
#~ "Include all applications and operating system kernel in callgraph. This "
#~ "may not be possible on some system configurations."
#~ msgstr ""
#~ "Inkludera alla program och operativsystemets kärna i anropsgraf. Detta är "
#~ "kanske inte möjligt för vissa systeminställningar."

#~ msgid "Enable to launch a program of your choosing before profiling."
#~ msgstr "Aktivera körning av ett valfritt program innan profilering."

#~ msgid "Environment"
#~ msgstr "Miljö"

#~ msgid ""
#~ "If disabled, your CPU will be placed in performance mode. It will be "
#~ "restored after profiling."
#~ msgstr ""
#~ "Om inaktiverad kommer din processor att placeras i prestandaläge. Den "
#~ "kommer att återställas efter profilering."

#~ msgid "Window size"
#~ msgstr "Fönsterstorlek"

#~ msgid "Window size (width and height)."
#~ msgstr "Fönsterstorlek (bredd och höjd)."

#~ msgid "Window position"
#~ msgstr "Fönsterposition"

#~ msgid "Window position (x and y)."
#~ msgstr "Fönsterposition (x och y)."

#~ msgid "Window maximized"
#~ msgstr "Fönster maximerat"

#~ msgid "Window maximized state"
#~ msgstr "Maximerat tillstånd för fönster"

#~ msgid "Cumulative"
#~ msgstr "Kumulativt"

#~ msgid "Welcome to Sysprof"
#~ msgstr "Välkommen till Sysprof"

#~ msgid "Start profiling your system with the <b>Record</b> button above"
#~ msgstr "Börja profilera ditt system med knappen <b>Spela in</b> ovan"

#~ msgid "Search"
#~ msgstr "Sök"

#~ msgid "Key"
#~ msgstr "Nyckel"

#~ msgid "Value"
#~ msgstr "Värde"

#~ msgid "New Process"
#~ msgstr "Ny process"

#~ msgid "00:00"
#~ msgstr "00:00"

#~ msgid ""
#~ "Sysprof requires authorization to access your computers performance "
#~ "counters."
#~ msgstr ""
#~ "Sysprof kräver autentisering för att komma åt din dators prestandaräknare."

#~ msgid "CPU"
#~ msgstr "CPU"

#~ msgid "FPS"
#~ msgstr "Bilder/s"

#~ msgid "%u Process"
#~ msgid_plural "%u Processes"
#~ msgstr[0] "%u process"
#~ msgstr[1] "%u processer"

#~ msgid "The command line arguments provided are invalid"
#~ msgstr "Kommandoradsargumenten som tillhandahölls är ogiltiga"

#~ msgid "Not running"
#~ msgstr "Kör inte"

#~ msgid "_Close"
#~ msgstr "S_täng"

#~ msgid "Zoom out (Ctrl+-)"
#~ msgstr "Zooma ut (Ctrl+-)"

#~ msgid "Reset zoom level (Ctrl+0)"
#~ msgstr "Återställ zoomnivå (Ctrl+0)"

#~ msgid "Zoom in (Ctrl++)"
#~ msgstr "Zooma in (Ctrl++)"

#~ msgctxt "menu label"
#~ msgid "Open Capture…"
#~ msgstr "Öppna fångst…"

#~ msgid "Save As…"
#~ msgstr "Spara som…"

#~ msgid "Screenshot"
#~ msgstr "Skärmbild"

#~ msgid "Samples: %u"
#~ msgstr "Samplingar: %u"

#~ msgid "%s — %s"
#~ msgstr "%s — %s"

#~ msgid "Stop"
#~ msgstr "Stoppa"

#~ msgid "Building profile…"
#~ msgstr "Bygger profil…"

#~ msgid "Stopping…"
#~ msgstr "Stoppar…"

#~ msgid "Save Capture As…"
#~ msgstr "Spara fångst som…"

#~ msgid "An error occurred while attempting to save your capture: %s"
#~ msgstr "Ett fel uppstod vid försök att spara din fångst: %s"

#~ msgid "The file “%s” could not be opened. Only local files are supported."
#~ msgstr "Filen ”%s” kunde inte öppnas. Endast lokala filer stöds."

#~ msgid "sysprof"
#~ msgstr "sysprof"

#~ msgid "About"
#~ msgstr "Om"

#~ msgid "_Quit"
#~ msgstr "A_vsluta"

#~ msgid "%s - %s"
#~ msgstr "%s - %s"
